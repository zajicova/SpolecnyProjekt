

public class Auto
{
    private String barva, znacka;
    private int rokVyroby;
    
    public Auto(String barva, String znacka, int rokVyroby){
        this.barva = barva;
        this.znacka = znacka;
        this.rokVyroby = rokVyroby;
    }
}
