import java.util.*;

public class KvadratickaRovnice {
    private static Scanner in = new Scanner(System.in);
    
    public static void rovnice() {
        System.out.print("Zadej a: ");
        int a = in.nextInt();
        System.out.print("Zadej b: ");
        int b = in.nextInt();
        System.out.print("Zadej c: ");
        int c = in.nextInt();
        double d = Math.pow(b,2)-(double)(4*a*c);
        if(d<0){
            System.out.println("Nemá řešení");
        }else if(d==0) {
            double x = -b/(2*a);
            System.out.println("x = " + x);
        }else {
            double x1 = (-b+Math.sqrt(d))/(2*a);
            double x2 = (-b-Math.sqrt(d))/(2*a);
            System.out.println("x1 = " + x1 + " a x2 = " + x2);
        }
   }
}
