import java.util.Scanner;

public class SpolecnyPriklad {
    private static Scanner in = new Scanner(System.in);
    
    public static void sporeni() {
        System.out.print("Zadej částku, kterou chceš naspořit: ");
        int castka = in.nextInt();
        int pocetMesicu = 0;
        while(castka>0) {
            System.out.print("Zadej měsíční vklad: ");
            int vklad = in.nextInt();
            castka -= vklad;
            pocetMesicu++;
        }
        
        System.out.print("Doba spoření je: " + pocetMesicu);
    }
}
