import java.util.*;

public class KvadratickaRovnice
{
    private static Scanner sc = new Scanner(System.in);
    
    public static void rovnice()
    {
        System.out.print("Zadejte číslo a: ");
        int a = sc.nextInt();
        System.out.print("Zadejte číslo b: ");
        int b = sc.nextInt();
        System.out.print("Zadejte číslo c: ");
        int c = sc.nextInt();
        double d = Math.pow(b, 2)-(double)(4*a*c);
        if(d<0)
        {
            System.out.print("Rovnice nemá žádné řešení, protože je diskriminant menší než 0.");
        }
        else if(d==0)
        {
            double x=-b/(2*a);
        }
        else
        {
            double x1=(-b+Math.sqrt(d))/(2*a);
            double x2=(-b-Math.sqrt(d))/(2*a);
            System.out.println("x1 je "+x1+" a x2 je "+x2+".");
        }
    }
}
