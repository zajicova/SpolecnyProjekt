import java.util.*;

public class Clovek
{
    private String jmeno;
    private String prijmeni;
    private int vek;
    
    public Clovek(String jmeno, String prijmeni, int vek)
    {
        this.jmeno=jmeno;
        this.prijmeni=prijmeni;
        this.vek=vek;
    }
    public void setJmeno(String jmeno)
    {
        this.jmeno=jmeno;
    }
    public void setPrijmeni(String prijmeni)
    {
        this.prijmeni=prijmeni;
    }
    public void setVek(int vek)
    {
        this.vek=vek;
    }
    public String getJmeno()
    {
        return this.jmeno;
    }
    public String getPrijmeni()
    {
        return this.prijmeni;
    }
    public int getVek()
    {
        return this.vek;
    }
}
