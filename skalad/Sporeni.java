import java.util.*;

public class Sporeni
{
    private static Scanner sc = new Scanner(System.in);
    
    public static void sporeni()
    {
        System.out.print("Zadejte částku, kterou chcete vložit: ");
        int castka = sc.nextInt();
        int pocetMesicu=0;
        while(castka>0)
        {
            System.out.print("Zadejte měsíční vklad: ");
            int vklad = sc.nextInt();
            castka -= vklad;
            pocetMesicu++;
        }
        System.out.println("Doba spoření (počet měsíců) je "+pocetMesicu+".");
    }
}
