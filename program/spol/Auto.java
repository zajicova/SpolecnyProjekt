

public class Auto
{
    String barva, znacka;
    int rok;
    
    public Auto(String barva, String znacka, int rok)
    {
        this.barva = barva;
        this.znacka = znacka;
        this.rok = rok;
    }
    
    public String getBarva()
    {
        return barva;
    }
    
    public int getRok()
    {
        return rok;
    }
    
        public String getZnacka()
    {
        return znacka;
    }
    
    public void setBarva(String Barva)
    {
        this.barva = barva;
    }
    
    public void setRok(int rok)
    {
        this.rok=rok;
    }
    
      public void setZnacka(String znacka)
    {
        this.znacka = znacka;
    }
}
