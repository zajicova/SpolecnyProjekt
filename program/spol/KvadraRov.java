import java.util.*;

public class KvadraRov
{
    public static Scanner sc= new Scanner(System.in);
    
    public static void rovnice()
    {
        System.out.print(" Zadej číslo a:");
        int a = sc.nextInt();
        System.out.print("Zadej číslo b:");
        int b = sc.nextInt();
        System.out.print("Zadej číslo c:");
        int c = sc.nextInt();
        double d = Math.pow(b, 2)-(double)(4*a*c);
        if (d<0)
        {
            System.out.print("Rovnice nemá řešení.");
            
        }
        else if(d==0)
        {
            double x = -b/(2*a);
            System.out.print("Rovnice má jedno řešení: "+x+".");
        }
        else
        {
            double x1 = (-b+Math.sqrt(d))/2*a;
            double x2 = (-b-Math.sqrt(d))/2*a;
            System.out.print("Rovnice má dva kořeny první:"+x1+" druhý "+x2+".");
        }
    }
}
