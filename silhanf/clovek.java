

public class clovek
{
    String jmeno, prijmeni;
    int vek;
    
    public clovek(String jmeno, String prijmeni, int vek)
    {
        this.jmeno = jmeno;
        this.prijmeni = prijmeni;
        this.vek = vek;
    }
    
    public String getJmeno()
    {
        return jmeno;
    }
    
    public String getPrijmeni()
    {
        return prijmeni;
    }
    
    public int getVek()
    {
        return vek;
    }
    
    public void setJmeno(String jmeno)
    {
        this.jmeno = jmeno;
    }
    
    public void setPrijmeni(String prijmeni)
    {
        this.prijmeni = prijmeni;
    }
    
    public void setVek(int vek)
    {
        this.vek = vek;
    }
}
