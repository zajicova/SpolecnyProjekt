

public class auto
{
    String barva, znacka;
    int rok;
    
    public auto(String barva, String znacka, int rok)
    {
        this.barva = barva;
        this.znacka = znacka;
        this.rok = rok;
    }
    
    public String getBarva()
    {
        return barva;
    }
    
    public String getZnacka()
    {
        return znacka;
    }
    
    public int getRok()
    {
        return rok;
    }
    
    public void setBarva(String barva)
    {
        this.barva = barva;
    }
    
    public void setZnacka(String znacka)
    {
        this.znacka = znacka;
    }
    
    public void setRok(int rok)
    {
        this.rok = rok;
    }
}
